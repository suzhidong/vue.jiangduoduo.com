const path = require('path');
// eslint-disable-next-line import/no-extraneous-dependencies
const pxtorem = require('postcss-pxtorem');

const sourceSrc = path.resolve(__dirname, './src/module');

const { defineConfig } = require('@vue/cli-service');
const configureWebpack = require('./webpack.config');

const isProduction = process.env.NODE_ENV === 'production';
const publicPath = isProduction ? process.env.prefix : '/';

const BRAND_CONFIG = {
  id: 'duoduoyoucai',
  name: '奖多多',
  nameShort: '奖多多',
  /**
   * 一级域名
   * @type {String}
   */
  TDL: 'duoduoyoucai.com',
  /**
   *  客服code
   * @type {String}
   */
  serviceCode: 'd62897edbc9077eeb0e3c9b306b6637d',
  /**
   * IOS Schema
   */
  iosSchema: 'szcapp',
  /**
   * Android Schema
   */
  androidSchema: 'szcapp',
};

module.exports = defineConfig({
  publicPath: publicPath,
  transpileDependencies: true,
  configureWebpack,
  css: {
    loaderOptions: {
      postcss: {
        postcssOptions: {
          // 在plugins外面加一层postcssOptions
          plugins: [
            pxtorem({
              rootValue: 100,
              propList: ['*'],
            }),
          ],
        },
      },
    },
  },
  chainWebpack(webpackConfig) {
    webpackConfig.plugin('define').tap((options) => {
      options[0] = {
        ...options[0],
        BRAND_CONFIG: JSON.stringify(BRAND_CONFIG),
      };
      return options;
    });
  },
  pluginOptions: {
    'muti-dev': {
      sourceSrc,
      debug: true,
    },
    'muti-build': {
      sourceSrc,
      debug: false,
      report: false, // 生成analyzer 报告
      modern: false, // 是否启用现代模式（文件数double）
      // eslint-disable-next-line global-require
      concurrency: Math.abs(require('os').cpus().length / 2), // 多核
    },
  },
});
