/* eslint-disable import/no-extraneous-dependencies */
const babelParser = require('@babel/parser');
const { default: traverse } = require('@babel/traverse');
const fs = require('fs');

function has(ast, name) {
  let resBol = false;
  try {
    traverse(ast, {
      enter(path) {
        if (path.node.type === 'Identifier' && path.isIdentifier({ name })) {
          resBol = true;
        }
      },
    });
  } catch (e) {
    return resBol;
  }
  return resBol;
}

module.exports = (filePath, name) => {
  const fileInfo = fs.readFileSync(filePath, { encoding: 'utf-8' });
  const ast = babelParser.parse(fileInfo, {
    sourceType: 'module',
  });

  return has(ast, name);
};
