/* eslint-disable no-empty */
/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs');
const path = require('path');
const { done } = require('@vue/cli-shared-utils');
const ftp = require('basic-ftp');

module.exports = async () => {
  if (fs.existsSync(path.resolve(process.cwd(), './dist/sourcemap'))) {
    const client = new ftp.Client();
    try {
      await client.access({
        host: '172.16.248.25',
        port: '21',
        user: 'ftpadmin',
        password: '1234567899',
      });
      done('Upload sourcemap strat');
      await client.uploadFromDir(path.resolve(__dirname, '../../dist/sourcemap'), '/sourcemap');
      done('Upload sourcemap end');
    } catch (e) {
    } finally {
      client.close();
    }
  }
};
