/* eslint-disable import/no-extraneous-dependencies */
const inquirer = require('inquirer');
const inquirerAutocompletePrompt = require('inquirer-autocomplete-prompt');

inquirer.registerPrompt('autocomplete', inquirerAutocompletePrompt);

module.exports = (options) => {
  function search(answers, input) {
    return new Promise((resolve) => {
      resolve(options.filter((opt) => opt.toLowerCase().indexOf(input || '') > -1));
    });
  }

  return inquirer
    .prompt([
      {
        type: 'autocomplete',
        name: 'entry',
        message: 'Pick one or input!!!',
        source: search,
        pageSize: 5,
      },
    ])
    .then((res) => [res.entry]);
};
