/* eslint-disable no-unused-vars */
const newVueHasKey = require('../utils/newVueHasKey');

const isProduction = process.env.NODE_ENV === 'production';
const prefix = isProduction ? process.env.prefix : '';

const externals = [
  // todo 根据是否引入的第三方库判断
  {
    quote: {
      vue: 'Vue',
    },
    path: isProduction ? `${prefix}/static/js/vue.global.prod.js` : '/static/js/vue.global.js',
    match() {
      return true;
    },
  },
  {
    quote: {
      'vue-router': 'VueRouter',
    },
    path: `${prefix}/static/js/vue-router.global.prod.js`,
    match(entry) {
      return newVueHasKey(entry, 'router');
    },
  },
  {
    quote: {
      vuex: 'Vuex',
    },
    path: `${prefix}/static/js/vuex.global.prod.js`,
    match(entry) {
      return newVueHasKey(entry, 'store');
    },
  },
];

module.exports = (api, options) => {
  if (options.pages) {
    Object.values(options.pages).forEach((page) => {
      page.externalJs = externals.filter(({ match }) => match(page.entry)).map(({ path }) => path);
    });
  }
  api.configureWebpack(() => ({
    externals: externals.reduce((res, item) => ({ ...res, ...item.quote }), {}),
  }));
};
