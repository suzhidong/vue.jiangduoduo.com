/* eslint-disable import/no-extraneous-dependencies */
const { RawSource } = require('webpack-sources');

const pluginName = 'hello-world-plugin';
class HelloWorldPlugin {
  options;

  constructor(options) {
    this.options = options;
  }

  // eslint-disable-next-line class-methods-use-this
  apply(compiler) {
    // 生成一个文件
    compiler.hooks.emit.tap(pluginName, (compilation) => {
      compilation.assets['aa/hello.js'] = new RawSource('console.log(\'Hello world!\')');
      console.log('compilation.assets', Object.keys(compilation.assets));
    });
  }
}

module.exports = HelloWorldPlugin;
