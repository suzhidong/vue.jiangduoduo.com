/* eslint-disable class-methods-use-this */
/* eslint-disable guard-for-in */
// 定义插件：ReplaceTextWebpackPlugin
class ReplaceTextWebpackPlugin {
  // apply()在webpack启动过程中自动被调用，它接收一个compiler对象
  // 该对象包含此次构建过程中所有的配置信息，我们也是通过该对象注册构子函数
  apply(compiler) {
    // 通过compiler的hooks属性访问到emit钩子，通过tap方法注册一个钩子函数
    // tap()接收2该参数，1: 插件名称；2:挂载到钩子上的函数
    compiler.hooks.emit.tap('ReplaceTextWebpackPlugin', (compilation) => {
      // compilation===>可以理解为此次打包的上下文
      for (const name in compilation.assets) {
        //  console.log(name) // dis文件下所有文件名称
        //  console.log(compilation.assets[name].source())  //  dis文件下所有文件内容
        // 只针对js文件进行处理，去调注释
        const oldTxt = '天王盖地虎';
        const newTxt = '宝塔镇河妖';
        if (name.endsWith('.js')) {
          const contents = compilation.assets[name].source();
          let withoutComments = null;
          if (contents && contents.replace) {
            withoutComments = contents.replace(oldTxt, newTxt);
          } else {
            withoutComments = contents;
          }

          compilation.assets[name] = {
            source: () => withoutComments,
            size: () => withoutComments.length,
          };
        }
      }
    });
  }
}

module.exports = ReplaceTextWebpackPlugin;
