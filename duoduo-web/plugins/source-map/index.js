/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');

const isProduction = process.env.NODE_ENV === 'production';

module.exports = (api, options) => {
  if (!isProduction) {
    return;
  }
  options.productionSourceMap = true;
  api.chainWebpack((webpackConfig) => {
    webpackConfig.devtool(false);
    webpackConfig.plugin('SourceMapDevToolPlugin').use(webpack.SourceMapDevToolPlugin, [
      {
        filename: 'sourcemap/h5.duoduoyoucai.com/[file].map',
        publicPath: 'http://172.16.248.25:3333/',
      },
    ]);
  });
};
