module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  globals: {
    BRAND_CONFIG: 'readonly',
    location: 'readonly',
  },
  extends: [
    'plugin:vue/vue3-essential',
    'airbnb-base',
  ],
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    'vue',
  ],
  rules: {
    'linebreak-style': 'off',
    'no-console': 'off',
    'no-param-reassign': 'off',
    'import/no-unresolved': 'off',
    'import/extensions': 'off',
    'object-shorthand': 'off',
    'import/prefer-default-export': 'off',
    'no-plusplus': 'off',
    'no-useless-escape': 'off',
    'global-require': 'off',
    'no-unused-expressions': 'off',
    'no-underscore-dangle': 'off',
    'no-restricted-syntax': 'off',
    'max-len': 'off',
    'no-shadow': 'off',
    'import/no-mutable-exports': 'off',
    'consistent-return': 'off',
  },
};
