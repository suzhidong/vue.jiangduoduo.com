// eslint-disable-next-line import/no-extraneous-dependencies
const qiniu = require('qiniu'); // 引入七牛云的SDK
const getRepositoryModule = require('./getRepositoryModule');
const {
  qiniuConfig: { bucket, accessKey, secretKey },
  localConfig: { sourceSrc },
} = require('./config'); // 七牛云配置

const mac = new qiniu.auth.digest.Mac(accessKey, secretKey);

const config = new qiniu.conf.Config();
// 空间对应的机房
config.zone = qiniu.zone.Zone_cn_east_2;

// 是否使用https域名
// config.useHttpsDomain = true;
// 上传是否使用cdn加速
// config.useCdnDomain = true;
const formUploader = new qiniu.form_up.FormUploader(config);
const bucketManager = new qiniu.rs.BucketManager(mac, config);

// formUploader.putFile方法上传文件
// 第一个属性为上传凭证
// 第二个属性为上传文件要以什么命名  null 则随机命名
// 第三个为文件的相对地址， 相对为当前执行文件的地址
// 第四个属性putExtra， 应该是额外需要的参数，用new qiniu.form_up.PutExtra()获取
// 第五个为回调函数，respErr失败内容  respBody主体内容  respInfo信息内容
const putExtra = new qiniu.form_up.PutExtra();

function getUploadToken(scope) {
  const options = {
    scope: scope, // 必填, 七牛云控制台添加的空间名称
    expires: 7200, // expires非必填， 在不指定上传凭证的有效时间情况下，默认有效期为1个小时。expires单位为秒
    returnBody:
      '{"key":"$(key)","hash":"$(etag)","fsize":$(fsize),"bucket":"$(bucket)","name":"$(x:name)"}',
    // returnBody非必填， 有时候我们希望能自定义这个返回的JSON格式的内容，可以通过设置returnBody参数来实现。
  };
  const putPolicy = new qiniu.rs.PutPolicy(options); // 配置
  const uploadToken = putPolicy.uploadToken(mac); // 获取上传凭证
  return uploadToken;
}
const uploadToken = getUploadToken(bucket);

const fileList = getRepositoryModule(sourceSrc);
console.log('🚀 ~ file: qiniu.js:45 ~ fileList:', fileList);

// 获取七牛云上的资源列表的方法封装
function getQiniuImg(bucket, limit) {
  return new Promise((resolve, reject) => {
    bucketManager.listPrefix(bucket, limit, (respErr, respBody, respInfo) => {
      if (respInfo.statusCode === 200) {
        resolve(respBody);
      } else {
        reject(respInfo);
      }
    });
  });
}

// 批量删除七牛云上资源列表方法封装
function batchRemove(fileList) {
  return new Promise((resolve, reject) => {
    fileList.map((fileName) => {
      bucketManager.delete(bucket, fileName, (
        respErr,
        respBody,
        respInfo,
      ) => {
        if (respInfo.statusCode === 200) {
          resolve(respBody);
        } else {
          reject(respInfo);
        }
      });
      return null;
    });
  });
}

// 批量上传文件封装
function batchUploadFile(fileList) {
  return new Promise((resolve, reject) => {
    fileList.map((file) => {
      putExtra.mimeType = file.type;
      formUploader.putFile(
        uploadToken,
        file.pageName,
        file.filepath,
        putExtra,
        (respErr, respBody, respInfo) => {
          if (respInfo.statusCode === 200) {
            // console.log(respBody);
            resolve(respBody);
            // console.log(respBody);
            // 如果成功，这里内容便是 图片信息
          } else {
            console.log(respInfo);
            reject(respInfo);
          }
        },
      );
    });
  });
}

async function uploadQiniuyun() {
  /**
   * 1、获取该空间下所有资源列表
   */

  const bucketList = (await getQiniuImg(bucket)).items.map((item) => item.key);
  console.log('七牛云空间文件列表', bucketList);

  /**
   * 2、批量删除该空间下所有资源
   */
  if (bucketList.length > 0) {
    const ret = await batchRemove(bucketList);
    console.log('删除完毕', ret);
  }

  /**
   * 3、将本地资源批量上传qiuniuyun
   */
  const res = await batchUploadFile(fileList);
  console.log(res, '上传完毕');
}

uploadQiniuyun();
