// eslint-disable-next-line import/no-extraneous-dependencies
const Client = require('ssh2-sftp-client');
// eslint-disable-next-line import/no-extraneous-dependencies
const { run } = require('runjs');
const {
  qiniuConfig: { prefix }, serverConfig, serverConfig: { remoteDist }, localConfig: { sourceSrc },
} = require('./config');

const rawArgv = process.argv.slice(2);
const args = rawArgv.join(' ');

const client = new Client();
const config = { ...serverConfig };

(async () => {
  await run(`cross-env prefix=${prefix}  vue-cli-service muti-build ${args}`);
  try {
    await client.connect(config);
    client.on('upload', (info) => {
      console.log(`Uploaded：${info.source}`);
    });
    const result = await client.uploadDir(sourceSrc, remoteDist);
    await run('node qiniu/qiniu.js');
    console.log(result);
  } catch (error) {
    console.error(error);
  } finally {
    client.end();
  }
})();
