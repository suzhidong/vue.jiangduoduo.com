// eslint-disable-next-line import/no-extraneous-dependencies
const glob = require('glob');
const path = require('path');
// eslint-disable-next-line import/no-extraneous-dependencies
const mime = require('mime');

module.exports = (PAGES_PATH) => {
  const pages = [];
  glob.sync(`${PAGES_PATH}/**/*.*`).forEach((filepath) => {
    const pageName = path.relative(PAGES_PATH, filepath).replace(/\\/g, '/');
    const type = mime.getType(filepath);
    pages.push({
      pageName,
      filepath,
      type,
    });
  });
  return pages;
};
