import { createApp } from 'vue';
import globalMixin from 'public/mixins/global';
import App from './App.vue';

const app = createApp(App);
app.mixin(globalMixin);
app.mount('#app');
