/* eslint-disable import/no-dynamic-require */
import { createRouter, createWebHashHistory } from 'vue-router';
import Container from './router_container.vue';

const routes = [
  {
    path: '/',
    redirect: { path: '/pages/home/home' },
  },
  {
    path: '/pages',
    redirect: { name: 'Home' },
    component: Container,
    children: [
      {
        path: 'home/home',
        name: 'Home',
        meta: {
          title: '首页',
        },
        component: () => import('../pages/entry/home/home.vue'),
      },
      {
        path: 'kaijiang/kaijiang',
        name: 'Kaijiang',
        meta: {
          title: '开奖',
        },
        component: () => import('../pages/entry/kaijiang/kaijiang.vue'),
      },
      {
        path: 'community/community',
        name: 'Community',
        meta: {
          title: '社区',
        },
        component: () => import('../pages/entry/community/community.vue'),
      },

      {
        path: 'my/my',
        name: 'My',
        meta: {
          title: '我的',
        },
        component: () => import('../pages/entry/my/my.vue'),
      },
      {
        path: 'home-person/home-person',
        name: 'HomePerson',
        meta: {
          title: '个人主页',
        },
        component: () => import('../pages/home-person/home-person.vue'),
      },
    ],
  },
  {
    path: '/**',
    component: () => ('src/components/404/404.vue'),
  },
];

const router = createRouter({
  history: createWebHashHistory(), // hash路由模式
  // history: createWebHistory(),  // history路由模式
  routes,
});

export default router;
