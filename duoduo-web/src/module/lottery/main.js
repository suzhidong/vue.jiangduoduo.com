import '@/scripts/rem';
import { createApp } from 'vue';
import { Button } from 'vant';
import globalMixin from 'public/mixins/global';
import bridge from 'public/bridge/';
import App from './App.vue';
import router from './router/index';
import store from './store';
import 'public/utils/isdebug';
import 'lottery/assets/icon/index.css';

import NoData from '@/components/NoData/NoData';
import OpenApp from '@/components/OpenApp/OpenApp';

const app = createApp(App);
app.mixin(globalMixin);
app.use(bridge);

app.component('no-data', NoData);
app.component('open-app', OpenApp);

console.log('tag:天王盖地虎');

app.use(Button);
app.use(router).use(store).mount('#app');
