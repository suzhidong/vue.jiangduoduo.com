import { browser } from 'public/utils/browser';

let defaultNative = {};
if (browser().android) {
  defaultNative = require('./appcall/android').default;
} else if (browser().ios) {
  defaultNative = require('./appcall/ios').default;
}
export default defaultNative;
