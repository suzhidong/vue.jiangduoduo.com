// 模块化, modules对象里的key为命名空间名称, modules外则直接挂载在顶级命名空间下

import Interactive from './interactive';
import info from './modules/info';
import link from './modules/link';

// 模块化, modules对象里的key为命名空间名称, modules外则直接挂载在顶级命名空间下
const interactive = new Interactive({
  modules: {
    info,
    link,
  },
});

export default interactive;
