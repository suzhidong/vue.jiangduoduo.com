import { browser } from 'public/utils/browser';
import native from '../native';

export default {
  login() {
    if (browser().isH5) {
      console.log('H5登录');
    } else {
      return native.login();
    }
  },
};
