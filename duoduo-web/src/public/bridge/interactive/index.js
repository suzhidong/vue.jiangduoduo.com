function _merge(target, v) {
  for (const [kk, vv] of Object.entries(v)) {
    target[kk] = vv;
  }
}

function Interactive(options = {}) {
  if (options.modules) {
    for (const [k, v] of Object.entries(options.modules)) {
      if (!this[k]) this[k] = {};
      _merge(this[k], v);
    }
  }
  for (const [k, v] of Object.entries(options)) {
    if (k !== 'modules') this[k] = v;
  }
}

Interactive.prototype.install = function (app) {
  Object.defineProperties(app.config.globalProperties, { $bridge: { value: this, writable: false } });
};

export default Interactive;
