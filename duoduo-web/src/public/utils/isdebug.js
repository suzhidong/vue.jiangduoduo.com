import { getParamsCode } from './getParamsCode';

/**
 * 加载vconsole工具
 */
const loadVConsole = () => {
  const isdebug = getParamsCode('isdebug');
  if (!isdebug || isdebug !== '1') {
    return Promise.resolve(null);
  }
  return new Promise((resolve, reject) => {
    /**
     * import() 导入方式
     */
    // try {
    //   import('vconsole').then((vconsole) => {
    //     resolve(vconsole.default);
    //   });
    // } catch (error) {
    //   reject(error);
    // }

    /**
     * require.ensure() 导入方式
     */
    require.ensure(
      [],
      () => {
        try {
          const vconsole = require('vconsole');
          resolve(vconsole);
        } catch (error) {
          reject(error);
        }
      },
      'vconsole',
    );
  });
};

/**
 * 使用
 */
loadVConsole().then((VConsole) => {
  VConsole && new VConsole();
});
