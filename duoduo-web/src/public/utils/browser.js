/*
 * @Descripttion:
 * @Date: 2021-11-23 15:32:45
 * @LastEditors: Rail.Wang
 * @LastEditTime: 2022-04-29 19:02:22
 */
/**
 * 获取浏览器信息、是否在app中打开
 */
import { getParamsCode } from './getParamsCode';

export function browser() {
  const u = navigator.userAgent.toLowerCase();
  return {
    txt: u, // 浏览器版本信息
    version: (u.match(/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1], // 版本号
    msie: /msie/.test(u) && !/opera/.test(u), // IE内核
    mozilla: /mozilla/.test(u) && !/(compatible|webkit)/.test(u), // 火狐浏览器
    safari:
            /safari/.test(u)
            && !(u.indexOf('crios') > -1)
            && !(u.indexOf('android') > -1)
            && !/chrome/.test(u)
            && !(u.indexOf('qqbrowser') > -1), // 是否为safair
    chrome: /chrome/.test(u) || u.indexOf('crios') > -1, // 是否为chrome
    opera: /opera/.test(u), // 是否为oprea
    presto: u.indexOf('presto/') > -1, // opera内核
    webKit: u.indexOf('applewebkit/') > -1, // 苹果、谷歌内核
    gecko: u.indexOf('gecko/') > -1 && u.indexOf('khtml') === -1, // 火狐内核
    mobile: !!u.match(/applewebkit.*mobile.*/), // 是否为移动终端
    ios: !!u.match(/\(i[^;]+;( u;)? cpu.+mac os x/), // ios终端
    android: u.indexOf('android') > -1, // android终端
    iPhone: u.indexOf('iphone') > -1, // 是否为iPhone
    iPad: u.indexOf('ipad') > -1, // 是否iPad
    weixin: /micromessenger/.test(u), // 微信
    QQBrowse: u.indexOf(' QQ') > -1 || u.indexOf(' qq') > -1,
    baidu: /baidu/.test(u),
    webApp: !!u.match(/applewebkit.*mobile.*/) && u.indexOf('safari/') === -1, // 是否web应该程序，没有头部与底部,
    isApp:
            (u.indexOf('uni-app') > -1 && u.indexOf('html5plus') > -1)
            || getParamsCode('source') === 'app'
            || u.indexOf('jscp/ios') > -1
            || u.indexOf('jscp/android') > -1,
    isH5: getParamsCode('source') === 'h5',
    isNotApp:
            u.indexOf('uni-app') === -1
            && u.indexOf('html5plus') === -1
            && getParamsCode('source') !== 'app'
            && u.indexOf('jscp/ios') === -1
            && u.indexOf('jscp/android') === -1,
    isiOSApp: u.indexOf('jscp/ios') > -1,
    isNewApp:
            (u.indexOf('jscp/ios') > -1 || u.indexOf('jscp/android') > -1 || getParamsCode('source') === 'app')
            && !(u.indexOf('uni-app') > -1 && u.indexOf('html5plus') > -1),
  };
}
