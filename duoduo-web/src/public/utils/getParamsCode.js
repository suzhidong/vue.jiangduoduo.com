import Qs from 'qs';

/**
 *   获取url参数
 *   例：getParamsCode('id');
 */
export function getParamsCode(key) {
  const index = window.location.href.indexOf('?');
  if (index === -1) return {};
  const params = window.location.href.slice(index + 1);
  return Qs.parse(params)[key] || '';
}
