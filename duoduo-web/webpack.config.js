const path = require('path');

module.exports = {
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendors: {
          name: 'chunk-vendors',
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          chunks: 'initial',
        },
        common: {
          name: 'chunk-common',
          minChunks: 2,
          priority: -20,
          chunks: 'all',
          reuseExistingChunk: true,
        },
      },
    },
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
      public: path.resolve(__dirname, './src/public'),
      lottery: path.resolve(__dirname, './src/module/lottery'),
      hd: path.resolve(__dirname, './src/module/hd'),
      resources: path.resolve(__dirname, './src/assets'),
    },
  },
};
